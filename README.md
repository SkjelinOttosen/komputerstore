Komputer Store is an fictional online store that sells computers.
Customers have an account that they can use to complete a purchase. The amount in the account can be increased by transferring salary or by taking out a loan. The rules for getting a loan are that customers can only borrow up to 2 times the balance and that they can only borrow once per computer they buy.

The website is made with HTML, CSS and Vanilla Javascript.
The layout is built with CSS Grid and CSS Flexbox. The images used on the page are free stock images taken from https://unsplash.com/. The logo is made with free logo design (https://www.freelogodesign.org/).