// Completes the purchase 
export function buyProduct(selectedProduct, loanGrantedBefore){
    let computerPrice = selectedProduct.price;
    let computerName = selectedProduct.productName;
    let currentAmount = amount.innerHTML;

    // Checks if the balance is grater of equale of the computer price
    if(currentAmount >= computerPrice){
        amount.innerHTML  = (parseInt(amount.innerHTML,10) - parseInt(computerPrice,10)).toFixed(2); 
         // Resets to false for the opportunity to take out a new loan to buy a new computer
        //loanGrantedBefore= false;
        window.localStorage.setItem('loanGrantedBefore', false);
        alert(`Congratulations you just bought a ${computerName} for NOK ${computerPrice.toFixed(2)}.
        The item will be shipped tomorrow.`);
       
    }
    else{
        alert("Sorry but you can not afford to buy this item. Do you want to apply for a loan?")
    }
}
