import { products, getProduct } from '/js/products.js';
import { balance } from '/js/balance.js';
import { loan } from '/js/loan.js';
import { monthlySalary } from '/js/salary.js';
import { buyProduct } from '/js/buy.js';

// Clears the local storage 
// Local storage is used to store the variable loanGrantedBefore
window.localStorage.clear();

// Sets the selected product to null
let selectedProduct = null;

// Flag that track if the loan is granted. Resets to false again when a purchase is complete
let loanGrantedBefore = false;
window.localStorage.setItem('loanGrantedBefore', false);

// Sets the start balance for the account
const amount = document.getElementById('amount');
amount.innerHTML = balance.toFixed(2);

// Select box for the prdocuts
var select = document.getElementById("products");
for(var i = 0; i <products.length; i++)
{
    // Creates options for each element in the product array
    var el = document.createElement("option");
    el.textContent = products[i].productName;
    el.value = products[i].id;
    select.appendChild(el);
}

// Updates the product information to the selected product
function updateSelected(){
    let selectedProductId = select.options[select.selectedIndex].value;;
    selectedProduct = getProduct(parseInt(selectedProductId));
    document.getElementById("product-name").innerHTML=selectedProduct.productName;
    document.getElementById("product-image").src= `/images/products/${ selectedProduct.productImage }`;
    document.getElementById("price").innerHTML= selectedProduct.price + ",-";
    document.getElementById("description").innerHTML="<b>Descriptions:</b> <br/>"+ selectedProduct.description;
    document.getElementById("btn-buy").removeAttribute("hidden");
}
// Updates the product information to the selected product on the event  
select.onchange = e => updateSelected();

// Event for the loan button
const btnLoan = document.getElementById('btn-loan');
btnLoan.onclick = e => loan(loanGrantedBefore);

// Event for the salary button
const btnSalary = document.getElementById('btn-salary');
btnSalary.onclick = e => monthlySalary(5000);

// Event for the buy button
const btnBuy = document.getElementById('btn-buy');
btnBuy.onclick = e => buyProduct(selectedProduct,loanGrantedBefore);







