// Loan method
export function loan(loanGrantedBefore){
    // If the loan i not been granted for this purchase
    if(window.localStorage.getItem('loanGrantedBefore').match(false)){

        // Max loan is calulates a 2 x balance
        let maxLoan = (parseInt(amount.innerHTML,10)* 2).toFixed(2);

        //Input from user. Disired loan amount
        let wantedLoan = parseInt(prompt(`Please enter the desired loan amount (Max NOK ${maxLoan}).`,0),10);
        const regexOnlyNumbers = /^\d+$/;

        // Checks if the promt is cancelled or has empty fields
        if(!isNaN(wantedLoan)){
            if(regexOnlyNumbers.test(wantedLoan) && wantedLoan > 0){
                // Checks if the wanted loan is less than the maxloan
                if( wantedLoan <= maxLoan){
                    window.localStorage.setItem('loanGrantedBefore', true);
                    amount.innerHTML = (parseInt(amount.innerHTML,10) + parseInt(wantedLoan,10)).toFixed(2); 
                    alert(`You have been granted a loan of NOK ${wantedLoan}. The money will be transfer to your bank account within 1 day.`)
                }
                else{
                    alert(`You can only be granted a loan that is 2 x your balance.\nPlease apply again with a smaller loan amount.`)
                } 
            }
            else{
                alert("Please enter a valid number.")
            }       
        }     
    } 
    else{
        alert("Sorry, but you have already been granted a loan to buy a computer.")
    }
}