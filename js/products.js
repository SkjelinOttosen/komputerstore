// Products data soruce
export const products =[

    { 
        "id": 0,
        "productName": "Dell Inspiron 15 3000",
        "productImage": "dellInspiron.jpg",
        "price" :10189,
        "description": "Laptop - Intel Core i7 1065G7 Ice Lake, 15.6 TN matte 1920 × 1080, RAM 8GB DDR4, Intel Iris Plus Graphics, SSD 512GB, numeric keypad, backlit keyboard, webcam, fingerprint reader, WiFi 802.11ac, 3-cell battery, Windows 10 Pro (NBD)."
    },  
    { 
        "id": 1,
        "productName": "Dell Inspiron Chromebook 14",
        "productImage": "DellInspironChromebook.jpg",
        "price" :7490,
        "description": "Tablet or laptop, Dell Inspiron Chromebook 14 2-in-1 can be both. The PC has a stylish exterior in polished aluminum, a fast and versatile Core i3 processor, and included you get a stylus pen for full use of the Full HD touch screen."
    },
    { 
        "id": 2,
        "productName": "HP Spectre x360",
        "productImage": "HPSpectre.jpg",
        "price" :15995,
        "description": "The HP Specter x360 aw0350 2-in-1 Laptop combines the power of a laptop with the versatility of a tablet. The sleek and lightweight PC is equipped with a powerful battery that can give you up to 23 hours of use."
    },
    { 
        "id": 3,
        "productName": "Macbook Air 256 GB Gold",
        "productImage": "macbookAir.jpg",
        "price" :12790,
        "description": "Magnificent 13.3-inch Retina display, 1.1GHz QC 10th-gen Intel Core i3 processor, 256GB SSD and 8GB RAM, Battery life of up to 11 hours."
    }     
]

// Gets all products 
function getAllProducts(){
    return products;
}

// Gets product by id
export function getProduct(id){
    return getAllProducts()[id];
}